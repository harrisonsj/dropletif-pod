#
#  Be sure to run `pod spec lint DropletIF.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "DropletIF"
  s.version      = "1.0.14"
  s.summary      = "DropletIF Framework"
  s.homepage     = "https://thedropletapp.com"
  s.license      = { :type => 'Droplet License', :text => 'DropletIF is licensed under the Droplet License' }
  s.authors      = { 'The Droplet App' => 'contact@thedropletapp.com' }
  s.platform     = :ios, "9.0"
  s.swift_version = '4.1'
  s.ios.vendored_frameworks = 'DropletIF.framework'
  s.source        = { :http => 'https://thedropletapp.com/pods/DropletIF.framework.zip' }
  s.dependency 'Firebase/Core', "~> 5.0.1"
  s.dependency 'Firebase/Messaging', "~> 5.0.1"
  s.dependency 'Firebase/Auth', "~> 5.0.1"
  s.dependency 'Firebase/Database', "~> 5.0.1"
  s.dependency 'Firebase/Storage', "~> 5.0.1"
  s.dependency 'FBSDKCoreKit', "~> 4.33.0"
  s.dependency 'FBSDKLoginKit', "~> 4.33.0"
  s.dependency 'GeoFire', "~> 3.0.0"
  s.dependency 'GoogleMaps', "~> 2.7.0"
  s.dependency 'GooglePlaces', "~> 2.7.0"
  s.dependency 'libCommonCrypto', "~> 0.1.1"

end
